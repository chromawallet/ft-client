
function waitRqSend (rq) {
    return new Promise( (resolve, reject) => {
        rq.send( (error) => {
            if (error) reject(error);
            else resolve();
        });
    })
}

function register1() {
    const rq = gtx.newRequest([pub1]);
    rq.ft_register(desc1);
    rq.sign(priv1, pub1);
    return waitRqSend(rq);
}

function register2() {
    const rq = gtx.newRequest([pub2]);
    rq.ft_register(desc2);
    rq.sign(priv2, pub2);
    return waitRqSend(rq);
}

function issue1() {
    const rq = gtx.newRequest([issuerPub]);
    rq.ft_issue("USD", 10000, id1);
    rq.sign(issuerPriv, issuerPub);
    return waitRqSend(rq);
}

function xfer() {
    const rq = gtx.newRequest([pub1]);
    rq.ft_xfer(
        [[id1, "USD", 5000]],
        [[id2, "USD", 5000]]
    );
    rq.sign(priv1, pub1);
    return waitRqSend(rq);
}

async function run() {
    await register1();
    await register2();
    await issue1();
    await xfer();
}

run().then(() => console.log("all done"));