const Postchain = require('postchain-client');

const ACCOUNT_TYPE_NULL = 0;
const ACCOUNT_TYPE_SIMPLE = 1;
const ACCOUNT_TYPE_MULTI_SIG = 2;

class FTTransaction {
    constructor (signers, gtxClient, restClient) {
        this.rq = gtxClient.newRequest(signers);
        this.gtxClient = gtxClient;
        this.txID = null;
        this.restClient = restClient;
        this.txRID = null;
    }

    register (accountDesc) {
        if (!Buffer.isBuffer(accountDesc)) throw Error("AccountDESC should be a Buffer");
        this.rq.ft_register(accountDesc);
    }

    issue (issuerID, assetID, amount, recipientID) {
        this.rq.ft_issue(issuerID, assetID, amount, recipientID);
    }

    send (params) {
        const from = params.from;
        const to = params.to;
        const amount = params.amount;
        const assetID = params.assetID;
        const memo = params.memo;
        this.transfer(
            [[from, assetID, amount]],
            [[to, assetID, amount]],
            (memo) ? { memo } : undefined
        );
    }

    transfer (inputs, outputs, extra) {
        if (extra !== undefined) {
            this.rq.ft_transfer(inputs, outputs, extra);
        } else {
            this.rq.ft_transfer(inputs, outputs);
        }

    }

    rawCall (name, ...args) {
        this.rq.addCall(name, ...args);
    }

    getBufferForSigning () {
        return this.rq.getBufferToSign();
    }

    getDigestForSigning () {
        if (this.txRID) return this.txRID;
        const buffer = this.rq.getBufferToSign();
        this.txRID = Postchain.util.sha256(buffer);
        return this.txRID;
    }

    getTxRID () {
        return this.getDigestForSigning();
    }

    sign (privkey) {
        this.rq.sign(privkey);
    }

    addSignature (pubkey, signature) {
        this.rq.addSignature(pubkey, signature);
    }

    submit () {
        this.getDigestForSigning(); // make sure txRID is cached
        return new Promise( (resolve, reject) => {
            this.rq.send( (error) => {
                if (error) reject(error);
                else {
                    resolve(this.txRID);
                }
            });
        });
    }

    submitAndWaitConfirmation () {
        return this.submit().then( txrid => this._waitConfirmation(txrid) );
    }

    _waitConfirmation(txid) {
        return new Promise( (resolve, reject) => {
            setTimeout(() => {
                console.log("Polling ", txid.toString('hex'));
                this.restClient.status(txid, (err, res) => {
                    console.log("Got status response:", err, res);
                    if (err) reject(err);
                    else {
                        const {status} = res;
                        switch (status) {
                            case "confirmed": resolve(txid); break;
                            case "rejected": reject(Error("Message was rejected")); break;
                            case "unknown": reject(Error("Server lost our message")); break;
                            case "waiting":
                                this._waitConfirmation(txid).then(resolve, reject);
                                break;
                            default:
                                console.log(status);
                                reject(Error("got unexpected response from server"));
                        }
                    }
                });
            }, 511);
        });
    }
}

class FTClient {

    constructor (restClientURL, blockchainRID) {
        let restClient = null;
        if (typeof restClientURL === 'string') {
            restClient = Postchain.restClient.createRestClient(restClientURL);
        } else if (typeof restClient === 'object') {
            restClient = restClientURL;
        } else throw Error("Wrong argument type");

        this.restClient = restClient;
        this.blockchainRID = blockchainRID;

        this.gtxClient = Postchain.gtxClient.createClient(
            restClient,
            blockchainRID,
            ["ft_transfer", "ft_issue", "ft_register"]
        );
    }

    makeAccountDesc (data) {
        return Postchain.gtx.encodeValue(data)
    }

    makeNullAccountDesc(pubkey) {
        checkPublicKey(pubkey, 'pubkey');
        return this.makeAccountDesc([ACCOUNT_TYPE_NULL, this.blockchainRID, pubkey]);
    }

    makeSimpleAccountDesc(pubkey) {
        checkPublicKey(pubkey, 'pubkey');
        return this.makeAccountDesc([ACCOUNT_TYPE_SIMPLE, this.blockchainRID, pubkey]);
    }

    makeMultSigAccountDesc(thresh, pubkeys) {
        pubkeys.map(pubkey => checkPublicKey(pubkey, 'pubkey'));
        return this.makeAccountDesc([ACCOUNT_TYPE_MULTI_SIG, this.blockchainRID, thresh, pubkeys]);
    }

    makeAccountID (desc) {
        return Postchain.util.sha256(desc);
    }

    makeTransaction (signers) {
        return new FTTransaction(signers, this.gtxClient, this.restClient);
    }

    getBalance (accountID, assetID) {
        return new Promise( (resolve, reject) => {
            this.restClient.query(
                {type:'ft_get_balance', account_id: accountID.toString('hex'), asset_id: assetID},
                (error, result) => {
                    if (error) reject(error);
                    else {
                        resolve(result.balance);
                    }
                }
            );
        });
    }

    getHistory (accountID, assetID) {
        return new Promise( (resolve, reject) => {
            this.restClient.query(
                {type:'ft_get_history', account_id: accountID.toString('hex'), asset_id: assetID},
                (error, result) => {
                    if (error) reject(error);
                    else {
                        resolve(result);
                    }
                }
            );
        });
    }

}

function checkPublicKey(pubKey, pubKeyName) {
    if (!pubKey || !Buffer.isBuffer(pubKey) || pubKey.length !== 33) {
        console.error(pubKey);
        console.log(pubKey.length);
        throw new Error(`${pubKeyName} is not a valid pubKey:`);
    }
}

module.exports = FTClient;
