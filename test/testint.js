const FTClient = require('../src/client');
const restClient = require("postchain-client").restClient;
const gtxClient = require("postchain-client").gtxClient;
const  secp256k1 = require('secp256k1');
const randomBytes = require('crypto').randomBytes;

function makeKeyPair () {
    let privKey;

    do {
       privKey = randomBytes(32);
    } while (!secp256k1.privateKeyVerify(privKey));

    const pubKey = secp256k1.publicKeyCreate(privKey);
    return {pubKey, privKey};
}

const blockchainRID = Buffer.from("5eee4c6c676c1b3d7e594ab5db67c315a6c9ed134e6094d4b66310c4a6b8e748", "hex");
const rest = restClient.createRestClient(`http://localhost:7740`, 5);
const ftClient = new FTClient(rest, blockchainRID);

const issuerPub = Buffer.from("03f811d3e806e6d093a4bcce49c145ba78f9a4b2fbd167753ecab2a13530b081f8", "hex");
const issuerPriv = Buffer.from("3132333435363738393031323334353637383930313233343536373839303133", "hex");
const issuerID = ftClient.makeAccountID(ftClient.makeNullAccountDesc(issuerPub));

function mkUser () {
    const user = makeKeyPair();
    user.adesc = ftClient.makeSimpleAccountDesc(user.pubKey);
    user.aid = ftClient.makeAccountID(user.adesc);
    return user;
}

const user1 = mkUser();
const user2 = mkUser();

function registerAndIssue() {
    const tx = ftClient.makeTransaction([issuerPub]);
    tx.register(user1.adesc);
    tx.register(user2.adesc);
    tx.issue(issuerID, "USD", 100000, user1.aid);
    tx.sign(issuerPriv, issuerPub);
    return tx.submitAndWaitConfirmation();
}

function xfer(amount, memo) {
    const tx = ftClient.makeTransaction([user1.pubKey]);
    tx.send({
        from: user1.aid,
        to: user2.aid,
        amount: 5000,
        assetID: "USD",
        memo
    });
    tx.sign(user1.privKey, user1.pubKey);
    return tx.submitAndWaitConfirmation()
}

registerAndIssue().then( () => {
    return xfer(5000)
}).then( (txid) => {
    console.log(txid.toString('hex'));
    return xfer(5000, 'hi')
}).then( (txid) => {
    console.log(txid.toString('hex'));
    return ftClient.getHistory(user1.aid, "USD");
}).then( (history) => {
    console.log(history);
}).catch( err => {
    console.log("ERROR", err.toString())
});
